# Checklist Pidila

Référentiels et bonnes pratiques de la DILA pour ses sites internet, extranet et intranet.

## Description du projet

Les sites de la DILA (Direction de l’information légale et administrative) visent la conformité à plusieurs référentiels. Cette démarche implique plusieurs métiers, à plusieurs étapes, sur différentes thématiques.

Ce dépôt permet de déployer l’ensemble des critères concernés sur une page web et de pouvoir y effectuer des tris croisés afin que chaque intervenant puisse générer sa propre liste personnalisée.

## Procédure de mise à jour de la checklist

1. Placer les anciens fichiers xls et csv dans le répertoire archives 
2. Placer la version la plus récente du fichier xls à la racine du dépôt
3. Le convertir au format csv, lequel sera placé dans le répertoire website/data et nommé pidila.csv

## Installation

```bash
$ npm install
```

Mettre à jour les dépendences 

```bash
$ npm install && gulp add:vendors-js
```


## Conversion excel > csv (à tester)

- Ouvrir dans LibreOffice
- Enregistrer sous CSV en cochant "Éditer les filtres"
- Vérifier qu’on est en UTF-8, séparateur "," et le guillemet double pour encadrer le texte
- Une fois enregistré : ouvrir le CSV (en fait dans LibreOffice, le document ouvert est le CSV une fois que c’est enregistré)
- Importation : Supprimer les 3 colonnes qui ne servent à rien (C, D, F), ce sont des colonnes de "maintenance" dont l’avenir est incertain.
- Enregistrer à nouveau le CSV (les paramètres de filtres sont conservés à ce stade)
- Changer le nom du csv dans le config.js


