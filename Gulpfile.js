// //////////////////////////////////////// //
//
//    Tout construire   :   gulp build
//    Tout nettoyer     :   gulp clean
//
//    compiler sass     :   gulp make:css
//    compiler twig     :   gulp make:html
//    watch html et css :   gulp
//
// //////////////////////////////////////// //


//  Installation de sass et prérequis
// ======================================== */

var gulp         = require('gulp');
var del          = require('del');
var runSequence  = require('run-sequence');
var plumber      = require('gulp-plumber');



// variables de chemins
// ======================================== */

var baseURL = '/';

var paths = {
  destJS: 'website/scripts/vendors/',
};


// on récupère les dépendences js depuis node-modules
// ==================================================

// déplacer les fichiers javascripts depuis node_modules
gulp.task('move:vendors-js', function() {
  console.log(">>>>> deplacement des dépendences js");
  gulp.src('node_modules/requirejs/*.js')
    .pipe(gulp.dest(paths.destJS));
  gulp.src('node_modules/jquery/dist/jquery.min.js')
    .pipe(gulp.dest(paths.destJS));
  gulp.src('node_modules/jquery-csv/src/jquery.csv.min.js')
    .pipe(gulp.dest(paths.destJS));
  gulp.src('node_modules/dot/doT.min.js')
    .pipe(gulp.dest(paths.destJS));
});

// supprime tout les fichiers scampi
gulp.task('clean:vendors-js', function() {
  return del(['website/scripts/vendors']);
});

// metttre à jour les dependences
gulp.task('add:vendors', function(callback) {
  runSequence(
    'clean:vendors-js',
    ['move:vendors-js'],
    callback);
});



// Tâche par défaut
// ===========================================================

gulp.task('default',['add:vendors']);



