define([], function () {
  var FILE = "data/CL-DILA_2017-03-01.csv";

  var array_map = Array(
      '__PADDING__',      //  0
      'rubrique',         //  1
      'desc',             //  2
      'desc_origin',      //  3
      'level',            //  4
      'rgaa',        //  5
      'charte-tat', //  6
      'greenit',     //  7
      'opquast',     //  8
      'conception',       //  9
      'graphisme',        // 10
      'integration',      // 11
      'developpement',    // 12
      'redactionnel',     // 13
      'chef-de-projet'    // 14
  );

  var array_label = Array(
      '__PADDING__',                  //  0
      'thématique',                     //  1
      'Libellé',                      //  2
      'Test',                         //  3
      'niveau',                       //  4
      'RGAA',         //  5
      'Charte de l\'État',  //  6
      'Eco-conception',      //  7
      'Bonnes pratiques Opquast',      //  8
      'Conception',                   //  9
      'Graphisme',                    // 10
      'Intégration',                  // 11
      'Développement',                // 12
      'Rédactionnel',                 // 13
      'Gestion de projet'                // 14
  );


  var array_type = Array(
      '__PADDING__',  //  0
      'string',       //  1
      'string',       //  2
      'string',       //  3
      'string',       //  4
      'correspondance', //  5
      'correspondance', //  6
      'correspondance', //  7
      'correspondance', //  8
      'profile',      //  9
      'profile',      // 10
      'profile',      // 11
      'profile',      // 12
      'profile',      // 13
      'profile'       // 14
  );

  var array_sort = Array(
      '__PADDING__',  //  0
      'string',       //  1
      'string',       //  2
      'string',       //  3
      'number',       //  4
      'version',      //  5
      'rules',        //  6
      'string',       //  7
      'number',       //  8
      'string',       //  9
      'string',       // 10
      'string',       // 11
      'string',       // 12
      'string',       // 13
      'string'        // 14
  );

  var profile_columns = [9,10,11,12,13,14];
  var correspondance_columns = [5,6,7,8];

  var active_filters = [4,1];

  var on_ready = function() {

    /*
     * jQuery simple and accessible hide-show system (collapsible regions), using ARIA
     * @version v1.7.0
    */
    var attr_control = 'data-controls',
        attr_expanded = 'aria-expanded',
        attr_labelledby = 'data-labelledby',
        attr_hidden = 'data-hidden',
        $expandmore = $('.js-expandmore'),
        $body = $('body'),
        delay = 1500,
        hash = window.location.hash.replace("#", ""),
        multiexpandable = true,
        expand_all_text = 'Tout déplier',
        collapse_all_text = 'Tout replier';


    if ($expandmore.length) { // if there are at least one :)
        $expandmore.each(function(index_to_expand) {
            var $this = $(this),
                index_lisible = index_to_expand + 1,
                options = $this.data(),
                $hideshow_prefix_classes = typeof options.hideshowPrefixClass !== 'undefined' ? options.hideshowPrefixClass + '-' : '',
                $to_expand = $this.next(".js-to_expand"),
                $expandmore_text = $this.html();

            $this.html('<button type="button" class="' + $hideshow_prefix_classes + 'expandmore__button js-expandmore-button"><span class="expand-sign" aria-hidden="true"></span>' + $expandmore_text + '</button>');
            var $button = $this.children('.js-expandmore-button');

            $to_expand.addClass($hideshow_prefix_classes + 'expandmore__to_expand').stop().delay(delay).queue(function() {
                var $this = $(this);
                if ($this.hasClass('js-first_load')) {
                    $this.removeClass('js-first_load');
                }
            });

            $button.attr('id', 'label_expand_' + index_lisible);
            $button.attr(attr_control, 'expand_' + index_lisible);
            $button.attr(attr_expanded, 'false');

            $to_expand.attr('id', 'expand_' + index_lisible);
            $to_expand.attr(attr_hidden, 'true');
            $to_expand.attr(attr_labelledby, 'label_expand_' + index_lisible);

            // quick tip to open (if it has class is-opened or if hash is in expand)
            if ($to_expand.hasClass('is-opened') || (hash !== "" && $to_expand.find($("#" + hash)).length)) {
                $button.addClass('is-opened').attr(attr_expanded, 'true');
                $to_expand.removeClass('is-opened').removeAttr(attr_hidden);
            }
        });
    }


    $body.on('click', '.js-expandmore-button', function(event) {
        var $this = $(this),
            $destination = $('#' + $this.attr(attr_control));

        if ($this.attr(attr_expanded) === 'false') {

            if (multiexpandable === false) {
                $('.js-expandmore-button').removeClass('is-opened').attr(attr_expanded, 'false');
                $('.js-to_expand').attr(attr_hidden, 'true');
            }

            $this.addClass('is-opened').attr(attr_expanded, 'true');
            $destination.removeAttr(attr_hidden);
        } else {
            $this.removeClass('is-opened').attr(attr_expanded, 'false');
            $destination.attr(attr_hidden, 'true');
        }

        event.preventDefault();

    });

    $body.on('click keydown', '.js-expandmore', function(event) {
        var $this = $(this),
            $target = $(event.target),
            $button_in = $this.find('.js-expandmore-button');

        if (!$target.is($button_in) && !$target.closest($button_in).length) {

            if (event.type === 'click') {
                $button_in.trigger('click');
                return false;
            }
            if (event.type === 'keydown' && (event.keyCode === 13 || event.keyCode === 32)) {
                $button_in.trigger('click');
                return false;
            }

        }


    });

    $body.on('click keydown', '.js-expandmore-all', function(event) {
        var $this = $(this),
            is_expanded = $this.attr('data-expand'),
            $all_buttons = $('.js-expandmore-button'),
            $all_destinations = $('.js-to_expand');

        if (
            event.type === 'click' ||
            (event.type === 'keydown' && (event.keyCode === 13 || event.keyCode === 32))
        ) {
            if (is_expanded === 'true') {

                $all_buttons.addClass('is-opened').attr(attr_expanded, 'true');
                $all_destinations.removeAttr(attr_hidden);
                $this.attr('data-expand', 'false').html(collapse_all_text);
            } else {
                $all_buttons.removeClass('is-opened').attr(attr_expanded, 'false');
                $all_destinations.attr(attr_hidden, 'true');
                $this.attr('data-expand', 'true').html(expand_all_text);
            }
        }
    });

  };

  return {
    FILE: FILE,
    array_map: array_map,
    array_label: array_label,
    array_type: array_type,
    array_sort: array_sort,
    profile_columns: profile_columns,
    correspondance_columns: correspondance_columns,
    active_filters: active_filters,
    on_ready: on_ready
  };
});
