define([], function () {

    // utilise la virgule en séparateur de séries
    function string_to_comma(str) {
        return str.replace(/(?:\r\n|\r|\n)/g, ', ');
    }

    // utilise br en séparateur de séries
    function string_to_br(str) {
        return str.replace(/(?:\r\n|\r|\n)/g, '</p><p>');
    }

    // nettoie les chaînes de caractères pour paramètres
    function string_slugify(text) {
      if (text == "") {
        return 'empty';
      }
      return text.toString().toLowerCase()
        .replace(/\s+/g, '-')           // Replace spaces with -
        //.replace(/\?/g, 'inter-mark')    // Replace interrogation marks
        .replace(/[^\w\-]+/g, '')      // Remove all non-word chars
        .replace(/\-\-+/g, '-')         // Replace multiple - with single -
        .replace(/^-+/, '')             // Trim - from start of text
        .replace(/-+$/, '');            // Trim - from end of text
    }

    // transforme les caractères unicode en leurs entités numériques HTML
    function encodedString(rawStr) {
        return rawStr.replace(/[\u00A0-\u9999<>\&]/gim, function(i) {
           return '&#'+i.charCodeAt(0)+';';
        });
    }

    // construit un lien permanent vers la liste filtrée active
    function construct_permanent_link() {
        var params = {};
        $('input.filter_input:checked').each(function (i,elt) {
            params[elt.id]=1;
        });

        var recursiveEncoded = $.param( params );
        var location = document.location;
        $("#permanent_link").attr('href', location.protocol + '//' + location.host + location.pathname +'?'+ recursiveEncoded);
        $("#permanent_link").text('Lien permanent vers ces filtres');
    }

    return {
        str_to_comma: string_to_comma,
        str_to_br: string_to_br,
        slugify: string_slugify,
        encodedStr: encodedString,
        construct_link: construct_permanent_link
    }
})







// pas de test
function sortNumber(a, b) {
    return a - b;
}

// quand il y a le mot règle ça l'enlève puis ça "trie" - pas de test ?
function sortRule(a, b) {
    var regExStripRule = /^(Règle|règle) /;
    var stripped_a = a.replace(regExStripRule, '');
    var stripped_b = b.replace(regExStripRule, '');
    return stripped_a - stripped_b;
}


// tri -pas de test
function sortVersion(a, b) {
    var i, diff;
    var regExStrip0 = /(\.0+)+$/;
    var segmentsA = a.replace(regExStrip0, '').split('.');
    var segmentsB = b.replace(regExStrip0, '').split('.');
    var l = Math.min(segmentsA.length, segmentsB.length);

    for (i = 0; i < l; i++) {
        diff = parseInt(segmentsA[i], 10) - parseInt(segmentsB[i], 10);
        if (diff) {
            return diff;
        }
    }

    var lengthDiff = segmentsA.length - segmentsB.length;
    if(lengthDiff != 0)
        return lengthDiff
    return a.length - b.length;
}

// test : plus tard
function getParameterByName(name, url) {
    if (!url) {
      url = window.location.href;
    }
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
