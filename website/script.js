define(['doT', 'config', 'utils', 'jquery-csv'], function (doT, config, utils, $csv) {
  $(document).ready(function() {
      $.get(config.FILE,
          function(data) {

              var deferred = $.Deferred(function() {
                 printTable(data);
                 filter();
              });
              deferred.done(config.on_ready);

              deferred.resolve();

          },
          'text'
      );

      $("#link_box button").click(utils.construct_link);
  });

  // Load DoT JS template for the row
  var cell_tpl_fn = doT.template(document.getElementById('cell_tmpl').text);
  var PROFILE_FILTER = -1;
  var CORRESPONDANCE_FILTER = -2;
  var column_name = new Array();

  // -----------------------
  // |  Initialize filters |
  // -----------------------

  var filters = {};

  for (var item in config.active_filters) {
      filters[config.array_map[config.active_filters[item]]] = [];
  }

  // Init. filter for the column corresponding to the profiles.
  if (config.profile_columns.length != 0) {
      config.active_filters = config.active_filters.concat(config.profile_columns);
      filters['profile'] = [];

      for (var _index in config.profile_columns) {
          _profile_col_index = config.profile_columns[_index];
          _filter_object = {
            'label': config.array_label[_profile_col_index],
            'column_index': _profile_col_index,
            'column_id': config.array_map[_profile_col_index]
          };
          filters['profile'].push(_filter_object);
      }
  }

  // Init. filter for the column corresponding to the correspondances.
  if (config.correspondance_columns.length != 0) {
      config.active_filters = config.active_filters.concat(config.correspondance_columns);
      filters['correspondance'] = [];

      for (var _index in config.correspondance_columns) {
          _corres_col_index = config.correspondance_columns[_index];
          _filter_object = {
            'label': config.array_label[_corres_col_index],
            'column_index': _corres_col_index,
            'column_id': config.array_map[_corres_col_index]
          };
          filters['correspondance'].push(_filter_object);
      }
  }

  /* This method print a filter */
  function printFilter(label, filter_id, options, filter_index) {

  	var is_checked = false;
    var html_name = filter_id+'_filter';
  	var class_filter = 'class="filter_input"';

    var html_options = '<p class="filter-all"><input '+class_filter+' name="'+html_name+'" type="checkbox" value="ALL" checked="checked" id="'+filter_id+'_all"> <label for="'+filter_id+'_all">Tous</label></p>';

      if (filter_index != PROFILE_FILTER && filter_index != CORRESPONDANCE_FILTER) {
          if (config.array_sort[filter_index] == "number") {
              options.sort(sortNumber);
          } else if (config.array_sort[filter_index] == "version") {
              options.sort(sortVersion);
          } else if (config.array_sort[filter_index] == "rules") {
              options.sort(sortRule);
          } else {
              options.sort();
          }
      }

      for (var option in options) {
          var option_value = options[option];
          var value, html_id, html_label;
          if (option_value !== null && typeof option_value === 'object') {
              value = construct_class(filter_index, option_value);
              html_id = filter_id+'_'+value;
              html_label = ' <label for="'+html_id+'">'+option_value.label+'</label></p>';
          } else {
              value = construct_class(filter_index, option_value);
              html_id = filter_id+'_'+value;
              html_label = ' <label for="'+html_id+'">'+option_value+'</label></p>';
          }

  		    var check = "";

      		if (getParameterByName(html_id) == "1") {
      			 check = 'checked="checked"';
      			 is_checked = true;
      		}

          html_options += '<p class="filter"><input '+class_filter+' name="'+html_name+'" type="checkbox" value="'+value+'" id="'+html_id+'" '+check+'>'+html_label+'\n';
      }

      $('#filters').append('<fieldset class="filter-type fieldset-discrete"><legend class="h3">Filtrer par '+label+'</legend>'+html_options+"</fieldset>")
                   .find("[name='"+html_name+"']").change(filter);


      // ne semble pas fonctionner
      if (is_checked) {
  		    $("#"+filter_id+"_all").prop('checked', false);
  	   }
  }


  /* This method apply the filter */
  function filter(elm) {

      if (elm && $(elm.target).parents("p.filter-all").length > 0) {
         $(elm.target).parents("fieldset").find("p.filter input").prop('checked', false);
      }

      if (elm && $(elm.target).parents("p.filter").length > 0) {
         $(elm.target).parents("fieldset").find("p.filter-all input").prop('checked', false);
      }

      $("#contents article.row").hide();

      var rows = $("#contents article.row");
      var activated_filters = [];
      $("#filters fieldset").each(function() {

          var selected_classes = [];
          $("input:checked", this).each(function() {
              if ($(this).val() != "ALL") {
                  selected_classes.push('.' + $(this).val());
                  activated_filters.push($("label[for="+this.id+"]").text());
              }
          });
          if (selected_classes.length > 0) {
              rows = rows.filter(selected_classes.join(','));
          }
      });

      rows.show();
      $("#row-count").text($("#contents article.row:visible").length);
      if (activated_filters.length > 0) {
        $("#filter-actives").text("Filtres : " + activated_filters.join(', '));
      } else {
        $("#filter-actives").text("Aucun filtre n'a été appliqué pour le moment.");
      }
  }

  /* This method construct the class */
  function construct_class(column, value) {
      if ($.inArray(column, config.active_filters) !== -1) {
          if (config.array_type[column] == "array_string") {
              var values = value.split(/[\n]+/);
              values = values.filter(function(n){ return n != "" });

              var map_func = function(val, index) {
                  return config.array_map[column]+'-'+utils.slugify(val);
              }
              var array_classes = values.map(map_func);
              return array_classes.join(' ');
          } else if (config.array_type[column] == "profile" && value == "X") {
              return 'profile-'+config.array_map[column];
          } else if (config.array_type[column] == "correspondance" && value != "") {
              return 'correspondance-'+config.array_map[column];
          } else {
              return config.array_map[column]+'-'+utils.slugify(value);
          }

      } else if (column == PROFILE_FILTER) {
              return 'profile-'+value.column_id;
      } else if (column == CORRESPONDANCE_FILTER) {
              return 'correspondance-'+value.column_id;
      }

      return '';
  }


  /* This method print the CSV */
  function printTable(file) {
      /*
        columnRange
        Used in hook 'onParseValue' in order the build the content of filters
      */
      var columnRange = function(value, state) {
          // We save the column names (first row)
          if (state.rowNum == 1) {
              column_name[state.colNum] = value;
              return value;
          }

          // We store the encountered data of filtered columns in order to build the filters
          if ($.inArray(state.colNum, config.active_filters) !== -1 && $.inArray(state.colNum, config.profile_columns) == -1 && $.inArray(state.colNum, config.correspondance_columns) == -1) {
              if ($.inArray(value, filters[config.array_map[state.colNum]]) === -1) {
                  if (config.array_type[state.colNum] == "array_string") {
                      var values = value.split(/[\n]+/);
                      values = values.filter(function(n){ return n != "" });

                      var orig_array = filters[config.array_map[state.colNum]];
                      var union = [...new Set([...orig_array, ...values])];
                      filters[config.array_map[state.colNum]] = union;
                  } else {
                      filters[config.array_map[state.colNum]].push(value);
                  }
              }
          }

          return value;
      };

      /*
        apply_filters
        Used in hook 'onPostParse' in order the add the filters
      */
      var apply_filters = function(data) {
        if (config.profile_columns.length != 0) {
            printFilter('profil', 'profile', filters['profile'], PROFILE_FILTER);
        }
        if (config.correspondance_columns.length != 0) {
            printFilter('référentiel', 'correspondance', filters['correspondance'], CORRESPONDANCE_FILTER);
        }

        for (var item in config.active_filters) {
            var _column_index = config.active_filters[item];
            var _label = config.array_label[_column_index];
            var _id    = config.array_map[_column_index];

            if ($.inArray(_column_index, config.profile_columns) == -1 && $.inArray(_column_index, config.correspondance_columns) == -1) {
                printFilter(_label, _id, filters[config.array_map[_column_index]], _column_index);
            }
        }

        return data;
      };

      // Parse the CSV file and cut it into an array.
      var data = $.csv.toArrays(file, {
              onParseValue: columnRange, // On every parsed cell we have to check the content for the filters
              onPostParse: apply_filters // When the parsing is complete, we generate the filters
      });

      // Loop
      for(var row in data) {
        if (row == 0) continue;
        var li_classes = 'row'+row;
        var column = 1;
        var profiles = [];
        var correspondances = [];
        for(var item in data[row]) {
          var _value = $.trim(data[row][item]);
          // Generate classes for filtering
          li_classes += " " + construct_class(column, _value);

          // We store the associated profiles
          if ($.inArray(column, config.profile_columns) !== -1) {
              if (_value == "X") {
                  profiles.push(config.array_label[column]);
              }
          }
  		// We store the associated checklist
          if ($.inArray(column, config.correspondance_columns) !== -1) {
  			if (_value != "") {
  				correspondances.push(config.array_label[column]);
  			}
          }
          column++;
        }

        // Data for the template
        var tmp_data = {
            li_classes: li_classes,
            desc           : utils.encodedStr(data[row][1]),
            test           : utils.str_to_br(utils.encodedStr(data[row][2])),
      		category       : data[row][0],
      		level          : data[row][3],
      		corr_rgaa      : utils.str_to_comma(data[row][4]),
      		corr_charte    : utils.str_to_comma(data[row][5]),
      		corr_greenit   : utils.str_to_comma(data[row][6]),
      		corr_opquast   : utils.str_to_comma(data[row][7]),
      		profils        : profiles.join(", "),
      		correspondances: correspondances.join(", "),
      		label_rgaa     : config.array_label[5],
      		label_charte   : config.array_label[6],
      		label_greenit  : config.array_label[7],
      		label_opquast  : config.array_label[8]
  	    };

        // We pass the data to the template and get HTML code
  	    html_li = cell_tpl_fn(tmp_data);

        // Inject the generated HTML code (criterion) into the page.
        $("#contents").append(cell_tpl_fn(tmp_data));
      }
  }
});
